﻿using Entidades.Entities;
using Entidades.Enum;

namespace Dominio.Interfaces.InterfaceServices
{
    public interface ISaleService
    {
        private IList<Sale> SaleItem
        {
            get
            {
                return SaleItem.ToList();
            }
            set { }
        }

        void AddSale(Sale sale);
        List<Sale> GetSale();
        IList<Sale> GetById(int id);
        Sale UpdateSale(int id, SaleStatus newStatus);


    }
}
