﻿using System.Text.Json.Serialization;

namespace Entidades.Enum
{
    [JsonConverter(typeof(System.Text.Json.Serialization.JsonStringEnumConverter))]
    public enum SaleStatus : int
    {

        AwaitingPayment = 0,
        SentToCarrier = 1,
        Delivered = 2,
        Canceled = 3,
        PaymentAccept = 4
    }

}
