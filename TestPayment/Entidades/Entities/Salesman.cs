﻿namespace Entidades.Entities
{
    public class Salesman
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Cpf { get; set; }
        public string Mail { get; set; }
        public string CellphoneNumber { get; set; }
    }
}
