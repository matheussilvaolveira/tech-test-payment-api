﻿using Entidades.Enum;
using System.Data;

namespace Entidades.Entities
{
    public class Sale
    {
        public int Id { get; set; }
        public Salesman Salesman { get; set; }
        public DateTime SaleDate { get; set; }
        public List<Product> Itens { get; set; }

        public SaleStatus _status = SaleStatus.AwaitingPayment;

        public SaleStatus Status
        {
            get { return _status; }
            set { }
        }

        public Sale(int id, Salesman salesman, DateTime saleDate, List<Product> itens, SaleStatus status)
        {
            Id = id;
            Salesman = salesman;
            SaleDate = saleDate;
            Itens = itens;
            _status = status;
        }

        public Sale() { }
    }
}
