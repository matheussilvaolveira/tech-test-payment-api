﻿using Entidades.Entities;
using Entidades.Enum;

namespace Dominio.Function
{
    public class VerifyStatus
    {

        public void StatusVerify(Sale item, SaleStatus newStatus)
        {
            bool Ok = false;

            if (item.Status == SaleStatus.AwaitingPayment)
            {
                if (newStatus == SaleStatus.PaymentAccept || newStatus == SaleStatus.Canceled)
                    Ok = true;
                else
                    throw new Exception("Status só pode ser do tipo Payment Accept ou Canceled");

            }
            else if (item.Status == SaleStatus.PaymentAccept)
            {
                if (newStatus == SaleStatus.SentToCarrier || newStatus == SaleStatus.Canceled)
                    Ok = true;
                else
                    throw new Exception("Status só pode ser do tipo Send To Carrier ou Canceled");

            }

            else if (item.Status == SaleStatus.SentToCarrier)
            {
                if (newStatus == SaleStatus.Delivered)
                    Ok = true;
                else
                    throw new Exception("Status só pode ser do tipo Delivered.");
            }
        }

    }
}
