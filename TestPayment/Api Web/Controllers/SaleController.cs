﻿using Dominio.Interfaces.InterfaceServices;
using Entidades.Entities;
using Entidades.Enum;
using Microsoft.AspNetCore.Mvc;

namespace WebApiTest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SaleController : ControllerBase
    {

        private readonly ISaleService _sale;

        public SaleController(ISaleService sale)
        {
            _sale = sale;
        }

        [HttpPost]
        public IActionResult Create(Sale sale)
        {
            _sale.AddSale(sale);
            return Ok(sale);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var itens = _sale.GetSale();
            return Ok(itens);
        }

        [HttpGet("{id}")]
        public IActionResult GetByIdHttpGet(int id)
        {
            return Ok(_sale.GetById(id));
        }

        [HttpPut]
        public IActionResult UpdateStatus(int id, SaleStatus status)
        {
            if (_sale.GetSale().Count <= 0)
                return NotFound();

            var update = _sale.UpdateSale(id, status);
            return Ok(update);
        }
    }
}
