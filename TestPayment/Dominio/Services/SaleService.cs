﻿using Dominio.Function;
using Dominio.Interfaces.InterfaceServices;
using Entidades.Entities;
using Entidades.Enum;

namespace Dominio.Services
{
    public class SaleService : ISaleService
    {
        private readonly IList<Sale> SaleItem = new List<Sale>();

        public VerifyStatus verify = new VerifyStatus();

        public List<Sale> GetSale()
        {

            return SaleItem.ToList();
        }
        public IList<Sale> GetById(int id)
        {
            var getSale = SaleItem.Where(x => x.Id == id).ToList();
            return getSale;
        }
        public void AddSale(Sale sale)
        {
            SaleItem.Add(sale);
        }

        public Sale UpdateSale(int id, SaleStatus newStatus)
        {
            IList<Sale> list = SaleItem.Where(x => x.Id == id).ToList();

            Sale item = list[0];

            // Verifica o status do pedido.
            verify.StatusVerify(item, newStatus);


            Sale newItem = new Sale(item.Id, item.Salesman, item.SaleDate, item.Itens, newStatus);
            SaleItem.Remove(item);
            SaleItem.Insert(0, newItem);

            return newItem;
        }
    }
}
